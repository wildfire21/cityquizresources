package cat.itb.cityquiz.presentation.screens;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class CityQuizViewModel extends ViewModel {
    GameLogic gameLogic = RepositoriesFactory.getGameLogic();
    MutableLiveData<Game> gameMutableLiveData = new MutableLiveData<>();

    public void startGame() {
        Game game = gameLogic.createGame(Game.maxQuestions, Game.possibleAnswers);
        gameMutableLiveData.postValue(game);
    }

    public MutableLiveData<Game> getGame() {
        return gameMutableLiveData;
    }

    public void questionAnswered(int answer) {
        Game game = gameLogic.answerQuestions(gameMutableLiveData.getValue(), answer);
        gameMutableLiveData.postValue(game);
    }
}
