package cat.itb.cityquiz.presentation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;

import cat.itb.cityquiz.R;

import static androidx.navigation.Navigation.findNavController;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        NavigationUI.setupActionBarWithNavController(this,findNavController(this, R.id.nav_host_fragment));
    }

    @Override
    public boolean onSupportNavigateUp() {
        // Afegir si es vol mostrar el 'back' a la actionBar
        return findNavController(this, R.id.nav_host_fragment).navigateUp();
    }
}
