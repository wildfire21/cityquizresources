package cat.itb.cityquiz.presentation.screens.quiz;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.domain.Question;
import cat.itb.cityquiz.presentation.screens.CityQuizViewModel;

public class QuizFragment extends Fragment {

    @BindView(R.id.button1)
    Button button1;
    @BindView(R.id.button2)
    Button button2;
    @BindView(R.id.button3)
    Button button3;
    @BindView(R.id.button4)
    Button button4;
    @BindView(R.id.button5)
    Button button5;
    @BindView(R.id.button6)
    Button button6;
    @BindView(R.id.quizImage)
    ImageView quizImage;

    private CityQuizViewModel mViewModel;
    private Question currentQuestion;

    public static QuizFragment newInstance() {
        return new QuizFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.quiz_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(CityQuizViewModel.class);
        mViewModel.getGame().observe(this, this::display);
    }

    private void display(Game game) {
        if(!mViewModel.getGame().getValue().isFinished()) {
            currentQuestion = game.getCurrentQuestion();
            List<City> list = currentQuestion.getPossibleCities();

            String fileName = ImagesDownloader.scapeName(currentQuestion.getCorrectCity().getName());
            int resId = getContext().getResources().getIdentifier(fileName, "drawable", getContext().getPackageName());
            quizImage.setImageResource(resId);

            button1.setText(list.get(0).getName());
            button2.setText(list.get(1).getName());
            button3.setText(list.get(2).getName());
            button4.setText(list.get(3).getName());
            button5.setText(list.get(4).getName());
            button6.setText(list.get(5).getName());
        }
        else Navigation.findNavController(getView()).navigate(R.id.action_quizFragment_to_resultFragment2);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    private void questionAnswered(int answer, View view) {
        mViewModel.questionAnswered(answer);
        MutableLiveData<Game> game = mViewModel.getGame();

        /*if(!mViewModel.getGame().getValue().isFinished())
            display(game);
        else Navigation.findNavController(view).navigate(R.id.action_quizFragment_to_resultFragment2);*/
    }

    @OnClick({R.id.button1, R.id.button2, R.id.button3, R.id.button4, R.id.button5, R.id.button6})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button1:
                questionAnswered(0, view);
                break;
            case R.id.button2:
                questionAnswered(1, view);
                break;
            case R.id.button3:
                questionAnswered(2, view);
                break;
            case R.id.button4:
                questionAnswered(3, view);
                break;
            case R.id.button5:
                questionAnswered(4, view);
                break;
            case R.id.button6:
                questionAnswered(5, view);
                break;
        }
    }
}
