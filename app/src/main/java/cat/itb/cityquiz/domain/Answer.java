package cat.itb.cityquiz.domain;

import android.os.SystemClock;

public class Answer {
    City city;
    long time;

    public Answer(City city) {
        this.city = city;
        time = SystemClock.elapsedRealtime();
    }

    public Answer(City city, long time) {
        this.city = city;
        this.time = time;
    }
}
